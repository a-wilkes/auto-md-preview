import * as vscode from 'vscode';

export function activate(context: vscode.ExtensionContext) {

    let editor = vscode.window.activeTextEditor;
    if (editor) {
        let document = editor.document;
        openPreviewIfApplicable(document);
    }

    function openPreviewIfApplicable(document: vscode.TextDocument) {
        if (isMarkdownDocument(document)) {
            openMarkdownPreviewToSide();
        }
    }

    function isMarkdownDocument(document: vscode.TextDocument) : boolean {
        if (!document) return false;

        let markdown_id = "markdown";
        return document.languageId === markdown_id;
    }

    function openMarkdownPreviewToSide() {
        let openPreviewCommand = "markdown.showPreviewToSide"
        execute(openPreviewCommand);
    }

    function execute(command: string) {
        vscode.commands.executeCommand(command);
    }

    // Listeners
    vscode.workspace.onDidOpenTextDocument((document) => {
        openPreviewIfApplicable(document);
    });

    vscode.window.onDidChangeActiveTextEditor((editor) => {
        if (!editor) return;
        let document = editor.document;
        openPreviewIfApplicable(document);
    });
}

export function deactivate() {}
