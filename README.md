# Auto Markdown Preview

A simple extension that automatically opens the preview window to the side when viewing markdown files.

---
### [Repository](https://gitlab.com/a-wilkes/auto-md-preview)
